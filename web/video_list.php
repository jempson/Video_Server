<?php
include("../config/config.php");
include("include/function.php");
if (!Login_Status()) {
    header("Location:login.php");
    exit;
}
$redis=Redis_Link();
$db_link=DB_Link();
$result_video=mysqli_query($db_link,"SELECT * FROM video_list ORDER BY ID DESC");
$waiting=mysqli_num_rows(mysqli_query($db_link,"SELECT * FROM video_list WHERE status = '0'"));
$encoding=mysqli_num_rows(mysqli_query($db_link,"SELECT * FROM video_list WHERE status = '1'"));
$success=mysqli_num_rows(mysqli_query($db_link,"SELECT * FROM video_list WHERE status = '2'"));
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Video Encode Server</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            Video List
        </div>
        <div class="card-body">
            Waiting:<span class="badge badge-secondary"><?php echo $waiting;?></span>&nbsp;
            Encoding:<span class="badge badge-info"><?php echo $encoding;?></span>&nbsp;
            Success:<span class="badge badge-success"><?php echo $success;?></span>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Filename</th>
                <th scope="col">Add Time</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
                while ($row_video=mysqli_fetch_array($result_video)){
                    ?>
                    <tr>
                        <th scope="row"><?php echo $row_video['ID'];?></th>
                        <td><?php echo $row_video['filename'];?></td>
                        <td><?php echo date('Y-m-d H:i:s',$row_video['time']);?></td>
                        <td>
                            <?php
                            if ($row_video['status']==1){
                                echo '<span class="badge badge-info">Encoding</span>';
                            }elseif ($row_video['status']==2){
                                echo '<span class="badge badge-success">Success</span>';
                            }elseif ($row_video['status']==0){
                                echo '<span class="badge badge-secondary">Waiting</span>';
                            }
                            ?>
                        </td>
                    </tr>
            <?php
                }
            ?>
            </tbody>
        </table>
    </div>
</div>
</body>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</html>