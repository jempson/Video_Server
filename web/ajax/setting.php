<?php
include ("../../config/config.php");
include ("../include/function.php");
if ($_GET['action']=="update"){
    if ($_GET['type']=="encode"){
        if (!Login_Status()){
            $return['code']="101";
            $return['data']['message']="Login Status Error!";
            echo json_encode($return);
            exit;
        }
        $db_link=DB_Link();
        $redis=Redis_Link();
        $bitrate_video=mysqli_real_escape_string($db_link,$_POST['encode_bitrate_video']);
        $bitrate_audio=mysqli_real_escape_string($db_link,$_POST['encode_bitrate_audio']);
        $ts_time=mysqli_real_escape_string($db_link,$_POST['encode_ts_time']);
        $ts_frame=mysqli_real_escape_string($db_link,$_POST['encode_ts_frame']);
        $worker_thread=mysqli_real_escape_string($db_link,$_POST['worker_thread']);
        if (empty($bitrate_video)||empty($bitrate_audio)||empty($ts_frame)||empty($ts_time)||empty($worker_thread)){
            $return['code']="101";
            $return['data']['message']="Empty Value!";
            echo json_encode($return);
            exit;
        }
        mysqli_query($db_link,"UPDATE `setting` SET `data` = '".$bitrate_video."' WHERE `name` = 'encode_bitrate_video';");
        mysqli_query($db_link,"UPDATE `setting` SET `data` = '".$bitrate_audio."' WHERE `name` = 'encode_bitrate_audio';");
        mysqli_query($db_link,"UPDATE `setting` SET `data` = '".$ts_time."' WHERE `name` = 'encode_ts_time';");
        mysqli_query($db_link,"UPDATE `setting` SET `data` = '".$ts_frame."' WHERE `name` = 'encode_ts_frame';");
        mysqli_query($db_link,"UPDATE `setting` SET `data` = '".$worker_thread."' WHERE `name` = 'worker_thread';");
        $redis->del('Config_encode_bitrate_video','Config_encode_bitrate_audio','Config_encode_ts_time');
        $redis->del('Config_encode_ts_frame','Config_worker_thread');
        $return['code']="201";
        $return['data']['message']="Update Encode Config Successful!";
        echo json_encode($return);
        exit;
    }elseif ($_GET['type']=="api"){
        if (!Login_Status()){
            $return['code']="101";
            $return['data']['message']="Login Status Error!";
            echo json_encode($return);
            exit;
        }
        $db_link=DB_Link();
        $redis=Redis_Link();
        $api_key=mysqli_real_escape_string($db_link,$_POST['api_key']);
        mysqli_query($db_link,"UPDATE `setting` SET `data` = '".$api_key."' WHERE `name` = 'api_key';");
        $redis->del('Config_api_key');
        $return['code']="201";
        $return['data']['message']="Update API Config Successful!";
        if (empty($api_key)){
            $return['data']['message']="You set API Key empty.API will disable";
        }
        echo json_encode($return);
        exit;
    }
}