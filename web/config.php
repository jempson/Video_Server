<?php
include("../config/config.php");
include("include/function.php");
if (!Login_Status()) {
    header("Location:login.php");
    exit;
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Video Encode Server</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            Encode Config
        </div>
        <div class="card-body">
            <div class="alert alert-info"><strong>.ts File Encryption</strong> is Default Enable in This Version.</div>
            <div id="alert_encode"></div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="encode_bitrate_video">Video Bitrate</label>
                <div class="input-group col-sm-10">
                    <input class="form-control" id="encode_bitrate_video" placeholder="Video Bitrate"
                           value="<?php echo Get_Config('encode_bitrate_video'); ?>">
                    <div class="input-group-append">
                        <span class="input-group-text">Kbps</span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="encode_bitrate_video">Audio Bitrate</label>
                <div class="input-group col-sm-10">
                    <input class="form-control" id="encode_bitrate_audio" placeholder="Audio Bitrate"
                           value="<?php echo Get_Config('encode_bitrate_audio'); ?>">
                    <div class="input-group-append">
                        <span class="input-group-text">Kbps</span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="encode_ts_time">Time For .ts File</label>
                <div class="input-group col-sm-10">
                    <input class="form-control" id="encode_ts_time" placeholder="Time"
                           value="<?php echo Get_Config('encode_ts_time'); ?>">
                    <div class="input-group-append">
                        <span class="input-group-text">Seconds</span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="encode_ts_frame">Frame For .ts File</label>
                <div class="input-group col-sm-10">
                    <input class="form-control" id="encode_ts_frame" placeholder="Frame"
                           value="<?php echo Get_Config('encode_ts_frame'); ?>">
                    <div class="input-group-append">
                        <span class="input-group-text">Frames</span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="worker_thread">Encode Thread(s)</label>
                <div class="col-sm-10">
                    <input class="form-control" id="worker_thread" placeholder="Thread"
                           value="<?php echo Get_Config('worker_thread'); ?>">
                </div>
            </div>
            <div align="right">
                <button class="btn btn-outline-success btn-lg" onclick="Update_Config_Encode()">Submit</button>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            API Config
        </div>
        <div class="card-body">
            <div class="alert alert-danger"><strong>WARNING:</strong>This version's API not design for high performance application.</div>
            <div id="alert_api"></div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="api_key">API Key</label>
                <div class="input-group col-sm-10">
                    <input class="form-control" id="api_key" placeholder="API Key"
                           value="<?php echo Get_Config('api_key'); ?>">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" onclick="Random_API_Key(32)">Random</button>
                    </div>
                </div>
            </div>
            <div align="right">
                <button class="btn btn-outline-success btn-lg" onclick="Update_API_Encode()">Submit</button>
            </div>
        </div>
    </div>
</div>
</body>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    function Update_Config_Encode() {
        var encode_bitrate_video=document.getElementById('encode_bitrate_video').value;
        var encode_bitrate_audio=document.getElementById('encode_bitrate_audio').value;
        var encode_ts_frame=document.getElementById('encode_ts_frame').value;
        var encode_ts_time=document.getElementById('encode_ts_time').value;
        var worker_thread=document.getElementById('worker_thread').value;
        var ajax = new XMLHttpRequest();
        var alert = document.getElementById('alert_encode');
        ajax.open('POST','ajax/setting.php?action=update&type=encode',true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send('encode_bitrate_video='+encode_bitrate_video+'&encode_bitrate_audio='+encode_bitrate_audio+'&encode_ts_frame='+encode_ts_frame+'&encode_ts_time='+encode_ts_time+'&worker_thread='+worker_thread);
        ajax.onreadystatechange=function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var result = JSON.parse(ajax.responseText);
                if (result['code']==201){
                    alert.setAttribute('class','alert alert-success');
                    alert.innerHTML=result['data']['message'];
                }
                if (result['code']==101){
                    alert.setAttribute('class','alert alert-danger');
                    alert.innerHTML=result['data']['message'];
                }
            }
        }
    }
    function Update_API_Encode() {
        var api_key=document.getElementById('api_key').value;
        var ajax = new XMLHttpRequest();
        var alert = document.getElementById('alert_api');
        ajax.open('POST','ajax/setting.php?action=update&type=api',true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send('api_key='+api_key);
        ajax.onreadystatechange=function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                var result = JSON.parse(ajax.responseText);
                if (result['code']==201){
                    alert.setAttribute('class','alert alert-success');
                    alert.innerHTML=result['data']['message'];
                }
                if (result['code']==101){
                    alert.setAttribute('class','alert alert-danger');
                    alert.innerHTML=result['data']['message'];
                }
            }
        }
    }
    function Random_API_Key(len) {
        len = len || 32;
        var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        var maxPos = $chars.length;
        var pwd = '';
        for (var i = 0; i < len;i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        document.getElementById('api_key').value=pwd;
    }
</script>
</html>